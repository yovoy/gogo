from django.contrib.auth.forms import UserCreationForm
from django.views.generic import CreateView
from django.conf.urls import patterns, url
from django.utils.translation import ugettext as _

from accounts.forms import PasswordResetFormEdited, \
    SetPasswordFormEdited
from event.functions import get_fb_app_id

urlpatterns = patterns('',
    url(r'^message/(?P<code>[-\w]+)/$', 'accounts.views.message', name='message'),
    url('^register/', CreateView.as_view(
            template_name='accounts/register.html',
            form_class=UserCreationForm,
            success_url='/'
    ),name="register"),
    url(r'^get-or-create-user-with-fb/', 'accounts.views.get_or_create_user_with_fb', name='get_or_create_user_with_fb'),
)

urlpatterns += patterns('django.contrib.auth.views',
    url(r'^login/$', 'login', {
        'template_name': 'accounts/login.html',
        'extra_context': {
            'page': {
                'title': _('Log in'),
            },
            'FB_APP_ID': get_fb_app_id(),
        }
    }, name='login'),
    url(r'^logout/$', 'logout', {
        'next_page': '/',
    }, name='logout'),
    url(r'^profile/password/reset/$', 'password_reset', {
        'template_name': 'accounts/page_form.html',
        'password_reset_form': PasswordResetFormEdited,
        'post_reset_redirect':'/accounts/message/password_reset/',
        'email_template_name': 'accounts/email_reset.html',
        'extra_context': {
            'page': {
                'title': _('Reset password'),
            },
        }
    }, name='password_reset'),
    url(r'^profile/password/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$',
        'password_reset_confirm', {
            'template_name': 'accounts/page_form.html',
            'set_password_form': SetPasswordFormEdited,
            'post_reset_redirect': '/accounts/message/password_reset_confirm/',
        }, 'password_reset_confirm',),
)
