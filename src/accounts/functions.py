from django.contrib.auth.models import User

CREATED = 0
FOUND = 1
FOUND_INACTIVE = 2


def generate_username(base, correlative=''):
    username = base + str(correlative)
    try:
        User.objects.get(username=username)
        if correlative == '':
            correlative = 0
        correlative = int(correlative)
        correlative += 1
        return generate_username(username, correlative)
    except User.DoesNotExist:
        return username


def get_or_create_user_with_fb_util(facebook_user_form):
    email = facebook_user_form.cleaned_data['email']
    fb_id = facebook_user_form.cleaned_data['id']
    first_name = facebook_user_form.cleaned_data['first_name']
    last_name = facebook_user_form.cleaned_data['last_name']

    try:
        user = User.objects.get(email=email)
        if user.is_active:
            user.first_name = first_name
            user.last_name = last_name
            user.save()

            person = user.person
            person.first_name = user.first_name
            person.last_name = user.last_name
            person.email = user.email
            person.fb_id = fb_id
            person.save()

            return FOUND, user
        else:
            return FOUND_INACTIVE, user

    except User.DoesNotExist:
        user = User.objects.create_user(
            generate_username(first_name.replace(' ', '')),
            email,
            fb_id,
        )
        # user.pk = id
        user.first_name = first_name
        # TODO create a extendUser and add image_url param
        user.last_name = last_name
        # user.last_name = id
        # user.is_active = False
        user.save()

        person = user.person
        person.fist_name = user.first_name
        person.last_name = user.last_name
        person.email = user.email
        person.fb_id = fb_id
        person.save()

        return CREATED, user
