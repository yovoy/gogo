import json

from django.contrib.auth import authenticate, login
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt

from django.utils.translation import ugettext as _

from accounts.forms import *
from accounts.functions import get_or_create_user_with_fb_util, FOUND, FOUND_INACTIVE


def message(request, code):
    if code == 'password_reset':
        message = _(
            '<strong>We have sent you an email!</strong> Please, follow the instructions to reset your password.')
        # messages.add_message(request, messages.INFO, message)
        return render(request, 'accounts/echo.html', locals())
        # return redirect(reverse('accounts:password_reset'))

    if code == 'password_reset_confirm':
        messages.add_message(request, messages.INFO, _(
            '<strong>Success!</strong> You have changed your password.'))
        return redirect(reverse('accounts:login'))

    if code == 'password_change_done':
        messages.add_message(request, messages.INFO, _(
            '<strong>Success!</strong> You have changed your password.'))
        return redirect(reverse('accounts:password_change'))

    return redirect(reverse('accounts:profile_edit'))


@csrf_exempt
def get_or_create_user_with_fb(request):
    data = request.POST['data']
    json_loads = json.loads(data)

    facebook_user_form = FacebookUserForm(json_loads)
    if not facebook_user_form.is_valid():
        response_data = {
            'result': 'error',
            'message': 'fb user incorrect',
        }
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    fb_id = facebook_user_form.cleaned_data['id']

    response, user = get_or_create_user_with_fb_util(facebook_user_form=facebook_user_form)

    if response == FOUND:
        user = authenticate(username=user.username, password=fb_id)
        login(request, user)
        response_data = {
            'result': 'success',
            'message': 'welcome ' + user.first_name,
            'redirect_to': reverse('show_panel')
        }
    elif response == FOUND_INACTIVE:
        response_data = {'result': 'warning', 'message': 'your user account is being validating'}
    else:
        user = authenticate(username=user.username, password=fb_id)
        login(request, user)
        response_data = {
            'result': 'warning',
            'message': 'Ok!, welcome again ',
            'redirect_to': reverse('event:event_create')
        }

    return HttpResponse(json.dumps(response_data), content_type="application/json")
