��          �   %   �      p  9   q  8   �  (   �  c        q     �     �     �     �     �     �  #   �     �  
   �          	               %      <     ]     z     �     �     �     �     �  k  �  7   T  4   �  '   �  o   �     Y     m     �     �     �     �     �  (   �     �       	             0     8     D  &   c     �  !   �     �     �     �     �     	                                                                                       	                
                    <strong>Success!</strong> You have changed your password. <strong>Success!</strong> You have updated your profile. <strong>Thanks for joining us!</strong>. <strong>We have sent you an email!</strong> Please, follow the instructions to reset your password. Change password Create a new account Dear Edit profile Email Forgot your password? Log in Only alphanumeric values are valid. Password Password:  Reset Reset password Save Sign up Sincerely Our App Team Thanks for signing up in our app This email is already taken. This username is already taken. User name:  Username Welcome to our app You are logged in. Your user info is: Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-01-09 14:45-0500
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 <strong>¡Éxito!</strong> Has cambiado tu contraseña. <strong>¡Éxito</strong> Has actualizado tu perfil. <strong>¡Gracias por unirte!</strong>. <strong>¡Te hemos enviado un correo</strong> Por favor, sigue las instrucciones para recuperar tu contraseña. Cambiar contraseña Crear una nueva cuenta Estimado Editar perfil Email ¿Olvidaste tu contraseña? Iniciar sesión Solo valores alfanuméricos son validos. Contraseña Contraseña:  Recuperar Recuperar contraseña Guardar Registrarse Atentamente nuestro Equipo App Gracias por registrarte en nuestra app Este correo ya fue tomado. Este usuario no está disponible. Nombre de usuario:  Usuario Bienvenido a nuestro app Has iniciado sesión. Tu información de usuario es: 