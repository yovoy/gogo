from django import template
from django.core.urlresolvers import resolve

register = template.Library()

@register.filter
def is_checkbox(field):
    widget = field.field.widget
    class__ = widget.__class__
    return class__.__name__ == 'CheckboxInput'

@register.filter
def add_placeholder(field):
    field.field.widget.attrs.update({"placeholder": field.label})
    return field

@register.filter
def clear_upload_field(field):
    field.field.widget.template_with_clear = None
    return field

@register.filter
def presentation_start_date(presentation_group):
    presentation = presentation_group.__getitem__(0)
    return presentation.start


@register.filter
def is_class_image_upload(field):
    if field.field.widget.attrs.__contains__('class'):
        field_class = field.field.widget.attrs['class']
        if field_class.__contains__('image-upload'):
            return True
    return False

@register.filter
def name_without_spaces(field):
    return field.label.replace(' ','')

@register.filter
def get_lat(field_value):
    if not field_value:
        return 0
    if ',' in field_value:
        return field_value.split(',')[0]
    return field_value.split(' ')[0]



@register.filter
def get_lng(field_value):
    if not field_value:
        return 0
    if ',' in field_value:
        return field_value.split(',')[1]
    return field_value.split(' ')[1]


class ActiveUrlNode(template.Node):
    def __init__(self, request, names, return_value='active'):
        self.request = template.Variable(request)
        self.names = [template.Variable(n) for n in names]
        self.return_value = template.Variable(return_value)

    def render(self, context):
        request = self.request.resolve(context)
        any_of = False
        try:
            url = resolve(request.path_info)
            url_name = "%s:%s" % (url.namespace, url.url_name)
            for n in self.names:
                name = n.resolve(context)
                if url_name.startswith(name):
                    any_of = True
                    break
        except:
            # TODO - think a better way to log these
            print("Cannot resolve %s" % request.path_info)
        return self.return_value if any_of else ''

@register.tag
def active(parser, token):
    """
        Simple tag to check which page we are on, based on resolve;
        Useful to add an 'active' css class in menu items that needs to be
        aware when they are selected.

        Usage:

            {% active request "base:index" %}
            {% active request "base:index" "base:my_view" %}
    """
    try:
        args = token.split_contents()
        return ActiveUrlNode(args[1], args[2:])
    except ValueError:
        raise template.TemplateSyntaxError("%r tag requires at least 2 arguments" % token.contents.split()[0])