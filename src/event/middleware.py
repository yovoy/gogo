from django.http.response import HttpResponse
from django.utils import translation
from event.functions import get_current_event


class EventMiddleware(object):
    """
    This is a very simple middleware that parses a request
    and decides which website should be display according to
    the domain.
    """

    def process_request(self, request):
        # print(request.get_host())
        request.event = get_current_event(request)
