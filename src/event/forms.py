from django import forms
from django.utils.translation import ugettext as _

from event.functions import add_class, add_class_date_picker, add_class_time_picker
from event.widgets import AvatarWidgetNoLink

__author__ = 'bicho'

from django.forms import ModelForm

from event.models import Event, EventImage, Ticket, Person


class EventForm(ModelForm):
    form_meta = {
        'title': _('new event'),
        'help_collapse': True,
    }
    start = forms.DateField(widget=forms.DateInput(format='%d/%m/%Y'), input_formats=('%d/%m/%Y',))
    start_time = forms.TimeField(widget=forms.TimeInput(format='%I:%M %p'), input_formats=('%I:%M %p',))
    end = forms.DateField(widget=forms.DateInput(format='%d/%m/%Y'), input_formats=('%d/%m/%Y',), required=False)
    end_time = forms.TimeField(widget=forms.TimeInput(format='%I:%M %p'), input_formats=('%I:%M %p',), required=False)
    map_point = forms.CharField(error_messages={"required": "This field required you select a point on the map."})

    class Meta:
        model = Event
        exclude = ('owner', 'places', 'logo', 'slogan', 'theme', 'domain')

    def __init__(self, *args, **kwargs):
        super(EventForm, self).__init__(*args, **kwargs)
        add_class(self, ['name', 'address', 'nick', 'start_time', 'description'])
        add_class_date_picker(self, ['start', 'end'])
        add_class_time_picker(self, ['start_time', 'end_time'])

        self.fields['nick'].widget.attrs.update(
            {
                'placeholder': _('hashtag'),
            }
        )


class EventEditForm(ModelForm):
    form_meta = {
        'title': _('new event'),
        'help_collapse': True,
    }
    start = forms.DateField(label=_('start'), widget=forms.DateInput(format='%d/%m/%Y'), input_formats=('%d/%m/%Y',))
    start_time = forms.TimeField(label=_('at'), widget=forms.TimeInput(format='%I:%M %p'), input_formats=('%I:%M %p',))
    end = forms.DateField(label=_('end'), widget=forms.DateInput(format='%d/%m/%Y'), input_formats=('%d/%m/%Y',),
                          required=False)
    end_time = forms.TimeField(label=_('to'), widget=forms.TimeInput(format='%I:%M %p'), input_formats=('%I:%M %p',),
                               required=False)
    map_point = forms.CharField(error_messages={"required": "This field required you select a point on the map."})

    class Meta:
        model = Event
        exclude = (
            'owner', 'nick', 'places', 'theme')
        widgets = {
            'logo': AvatarWidgetNoLink,
        }

    def __init__(self, *args, **kwargs):
        super(EventEditForm, self).__init__(*args, **kwargs)
        add_class(self,
                  ['name', 'start_time', 'description', 'address', 'logo', 'slogan', 'facebook', 'twitter', 'domain'])
        add_class_date_picker(self, ['start', 'end'])
        add_class_time_picker(self, ['start_time', 'end_time'])


class EventImageForm(ModelForm):
    class Meta:
        model = EventImage
        fields = '__all__'


class TicketForm(ModelForm):
    class Meta:
        model = Ticket
        fields = ('id', 'event', 'name', 'quantity', 'cost')


class PersonForm(ModelForm):
    class Meta:
        model = Person
        fields = ('first_name', 'last_name', 'email')
