from django.conf.urls import patterns
from django.conf.urls import url
urlpatterns = patterns('event.views',
    url(r'^(?P<event_nick>\w+)/panel/$', 'home', name='home'),
    url(r'^nuevo/', 'event_create', name='event_create'),
    url(r'^panel/', 'show_panel', name='show_panel'),
    url(r'^(?P<event_nick>\w+)/editar/', 'event_edit', name='event_edit'),
    url(r'^(?P<event_nick>\w+)/scheme/', 'scheme', name='scheme'),
    url(r'^(?P<event_nick>\w+)/registers/', 'registers_json', name='registers_json'),
    url(r'^upload-image/', 'upload_image', name='upload_image'),
    url(r'^save-ticket/', 'save_ticket', name='save_ticket'),
    url(r'^delete-ticket/', 'delete_ticket', name='delete_ticket'),
    url(r'^(?P<event_nick>\w+)/inscritos/', 'registers', name='registers'),
    url(r'^(?P<event_nick>\w+)/registers-jqgrid/', 'registers_jqgrid', name='registers_jqgrid'),
    url(r'^(?P<event_nick>\w+)/registers-jqgrid-edit/', 'registers_jqgrid_edit', name='registers_jqgrid_edit'),

)

