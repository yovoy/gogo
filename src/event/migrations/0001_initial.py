# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import event.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.CharField(verbose_name='nombre', blank=None, max_length=100)),
                ('description', models.TextField(verbose_name='descripción', blank=True, null=True)),
                ('nick', models.SlugField(verbose_name='nombre corto', blank=None, max_length=100, unique=True)),
                ('domain', models.URLField(verbose_name='dominio', blank=True, null=True)),
                ('address', models.CharField(verbose_name='dirección', blank=True, max_length=200, null=True)),
                ('logo', models.ImageField(verbose_name='logo', upload_to='images', blank=True, null=True)),
                ('slogan', models.CharField(verbose_name='slogan', blank=True, max_length=200, null=True)),
                ('facebook', models.URLField(blank=True, max_length=255, null=True)),
                ('twitter', models.URLField(blank=True, max_length=255, null=True)),
                ('map_point', models.CharField(max_length=40)),
                ('map_place', models.CharField(max_length=250)),
                ('start', models.DateField(verbose_name='empieza')),
                ('start_time', models.TimeField(verbose_name='a horas', blank=True, null=True)),
                ('end', models.DateField(verbose_name='termina', blank=True, null=True)),
                ('end_time', models.TimeField(verbose_name='a horas', blank=True, null=True)),
                ('is_payed', models.BooleanField(verbose_name='vende entradas', default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='EventImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('image', models.ImageField(upload_to=event.models.content_file_name)),
                ('event', models.ForeignKey(related_name='images', to='event.Event')),
            ],
        ),
        migrations.CreateModel(
            name='EventPersonDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('is_dni', models.BooleanField(verbose_name='Solicitar DNI', default=False)),
                ('is_date_birth', models.BooleanField(verbose_name='Solicitar Fecha de Nacimiento', default=False)),
                ('is_sex', models.BooleanField(verbose_name='Solicitar Genero (hombre o mujer)', default=False)),
                ('is_institution', models.BooleanField(verbose_name='Solicitar Institución', default=False)),
                ('is_code', models.BooleanField(verbose_name='Solicitar Codigo institucional', default=False)),
                ('is_profession', models.BooleanField(verbose_name='Solicitar Profesión', default=False)),
                ('event', models.ForeignKey(to='event.Event')),
            ],
        ),
        migrations.CreateModel(
            name='Institution',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.CharField(verbose_name='nombre', blank=None, max_length=100)),
                ('fb_id', models.CharField(blank=True, max_length=100, null=True)),
                ('lat', models.FloatField(default=0)),
                ('lng', models.FloatField(default=0)),
                ('is_active', models.BooleanField(verbose_name='esta activo', default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='PageScheme',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('theme', models.CharField(blank=True, max_length=20, null=True)),
                ('scheme', models.TextField(blank=True, null=True)),
                ('main', models.TextField(blank=True, null=True)),
                ('nav', models.TextField(blank=True, null=True)),
                ('footer', models.TextField(blank=True, null=True)),
                ('event', models.ForeignKey(to='event.Event')),
            ],
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('email', models.EmailField(max_length=254)),
                ('fb_id', models.CharField(max_length=50)),
                ('facebook', models.URLField(blank=True, max_length=255, null=True)),
                ('linkedin', models.URLField(blank=True, max_length=255, null=True)),
                ('twitter', models.URLField(blank=True, max_length=255, null=True)),
                ('is_active', models.BooleanField(verbose_name='esta activo', default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('user', models.OneToOneField(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='PersonDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('dni', models.CharField(verbose_name='DNI', blank=True, max_length=20, null=True)),
                ('document', models.CharField(blank=True, max_length=50, null=True)),
                ('date_birth', models.DateField(verbose_name='Fecha de Nacimiento', blank=True, null=True)),
                ('sex', models.IntegerField(verbose_name='Sexo', blank=True, choices=[(1, 'hombre'), (2, 'mujer')], null=True)),
                ('profession', models.CharField(verbose_name='Profesion', blank=True, max_length=100, null=True)),
                ('person', models.ForeignKey(to='event.Person')),
            ],
        ),
        migrations.CreateModel(
            name='PersonInstitution',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('code', models.CharField(blank=True, max_length=20, null=True)),
                ('is_active', models.BooleanField(verbose_name='esta activo', default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('institution', models.ForeignKey(to='event.Institution')),
                ('person', models.ForeignKey(to='event.Person')),
            ],
        ),
        migrations.CreateModel(
            name='Register',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('voucher', models.CharField(verbose_name='recibos', blank=None, max_length=100)),
                ('status', models.IntegerField(default=1, choices=[(1, 'pendiente'), (2, 'confirmado')])),
                ('is_active', models.BooleanField(verbose_name='esta activo', default=False)),
                ('has_material', models.BooleanField(verbose_name='has material', default=False)),
                ('note', models.TextField(blank=True, null=True)),
                ('note2', models.TextField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('event', models.ForeignKey(to='event.Event')),
                ('person', models.ForeignKey(to='event.Person')),
                ('person_institution', models.ForeignKey(blank=True, to='event.PersonInstitution', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Ticket',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.CharField(verbose_name='nombre', blank=None, max_length=100)),
                ('description', models.CharField(verbose_name='descripción', blank=True, max_length=100, null=True)),
                ('quantity', models.IntegerField(verbose_name='cantidad', default=0)),
                ('cost', models.IntegerField(verbose_name='precio')),
                ('is_active', models.BooleanField(verbose_name='activo', default=True)),
                ('is_public', models.BooleanField(verbose_name='publico', default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('event', models.ForeignKey(to='event.Event')),
            ],
        ),
        migrations.AddField(
            model_name='register',
            name='ticket',
            field=models.ForeignKey(blank=True, to='event.Ticket', null=True),
        ),
    ]
