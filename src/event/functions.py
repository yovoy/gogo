from django.db.models import Q, ManyToManyField
from django.http.response import Http404
from event.models import Event
from gogo import settings

__author__ = 'bicho'


def add_class(form, fields):
    for field in fields:
        form.fields[field].widget.attrs.update(
            {
                "class": 'form-control',
                # 'placeholder' : form.fields[field].label
            }
        )


def add_class_date_picker(form, fields):
    for field in fields:
        form.fields[field].widget.attrs.update(
            {
                "class": 'form-control form-control-inline input-medium default-date-picker',
                'placeholder': 'dd/mm/yyyy'
            }
        )


def add_class_time_picker(form, fields):
    for field in fields:
        form.fields[field].widget.attrs.update(
            {
                "class": 'form-control timepicker-default',
                'placeholder': '00:00:00'
            }
        )


def add_class_image_upload(form, fields):
    for field in fields:
        form.fields[field].widget.attrs.update(
            {
                "class": 'image-upload default',
            }
        )


def get_event_or_404(id):
    try:
        return Event.objects.get(pk=id)
    except Event.DoesNotExist:
        raise Http404


def get_event_or_404_by_slug(slug):
    try:
        return Event.objects.get(nick=slug)
    except Event.DoesNotExist:
        raise Http404


def get_current_event(request):
    nick = request.get_host().lower()
    # print(nick)
    # Just for testing   somos.io
    nick = nick.replace(':8000', '')
    nick = nick.replace('.somos.io', '')
    nick = nick.replace('.yovoy.pe', '')
    nick = nick.replace('.crearevento.com', '')
    nick = nick.replace('.oneventus.com', '')
    # nick = nick.replace('http://', '')
    nick2 = 'http://' + nick.replace('www.', '')
    if nick2.endswith('/'):
        nick2 = nick2[:-1]

    try:
        event = Event.objects.get(
            Q(nick__iexact=nick) | Q(domain__iexact=nick) | Q(domain__iexact=nick2))
        return event
    except:
        pass
    return None


def get_current_event_or_404(request):
    if request.website:
        return request.website
    raise Http404


def get_array_dict_of_post_array(post, fields):
    fields_array = {}
    x = -1
    for field in fields:
        _field = field + '[]'
        if not post.get(_field):
            return None
        d = dict(post)[_field]
        fields_array[field] = d
        if x == -1:
            x = d.__len__()
        if x != d.__len__():
            raise Exception("fields size aren't equals")

    if x == -1:
        return None

    response = []
    for k in range(x):
        r = {}
        for field in fields:
            r[field] = fields_array.get(field)[k]
        response.append(r)
    return response


def get_fb_app_id():
    return settings.FB_APP_ID


def model_to_dict(instance):
    opts = instance._meta
    data = {}
    for f in opts.concrete_fields + opts.many_to_many:
        if isinstance(f, ManyToManyField):
            if instance.pk is None:
                data[f.name] = []
            else:
                data[f.name] = list(f.value_from_object(instance).values_list('pk', flat=True))
        else:
            data[f.name] = f.value_from_object(instance)
    return data


def get_datatable_data(request):
    field_order_by = 'id'
    query = '_all'
    if not request.POST:
        start = 0
        end = 10
    else:
        start = int(request.POST['start'])
        length = int(request.POST['length'])
        order = int(request.POST.get('order[0][column]'))
        query = request.POST.get('query')

        if order > 0:
            field_order_by = request.POST.get('columns[' + str(order) + '][data]')
            order_dir = request.POST.get('order[0][dir]')
            if order_dir != 'asc':
                field_order_by = '-' + field_order_by
        end = None
        if length > 0:
            end = start + length
    return end, field_order_by, query, start


def date_handler(obj):
    return obj.strftime('%Y-%m-%d') if hasattr(obj, 'isoformat') else obj
