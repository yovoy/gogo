from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from django.db import models


class Institution(models.Model):
    name = models.CharField(_('name'), max_length=100, blank=False, null=False)
    fb_id = models.CharField(max_length=100, blank=True, null=True)
    lat = models.FloatField(default=0)
    lng = models.FloatField(default=0)
    is_active = models.BooleanField(_('is active'), default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Event(models.Model):
    name = models.CharField(_('name'), max_length=100, blank=False, null=False)
    description = models.TextField(_('description'), blank=True, null=True)
    nick = models.SlugField(_('nick'), max_length=100, blank=False, null=False, unique=True)
    domain = models.URLField(_('domain'), null=True, blank=True)
    address = models.CharField(_('address'), max_length=200, blank=True, null=True)
    logo = models.ImageField(_('logo'), upload_to='images', blank=True, null=True)
    slogan = models.CharField(_('slogan'), max_length=200, blank=True, null=True)
    facebook = models.URLField(max_length=255, blank=True, null=True)
    twitter = models.URLField(max_length=255, blank=True, null=True)
    map_point = models.CharField(max_length=40)
    map_place = models.CharField(max_length=250)
    start = models.DateField(_('start'), null=False)
    start_time = models.TimeField(_('at'), null=True, blank=True)
    end = models.DateField(_('end'), null=True, blank=True)
    end_time = models.TimeField(_('to'), null=True, blank=True)
    is_payed = models.BooleanField(_('use tickets'), default=False)
    owner = models.ForeignKey(User, unique=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nick


Event.page_scheme = property(lambda e: PageScheme.objects.get_or_create(event=e)[0])
Event.tickets_public = property(lambda e: Ticket.objects.filter(event=e, is_active=True, is_public=True))
Event.tickets_all = property(lambda e: Ticket.objects.filter(event=e, is_active=True))
Event.person_details = property(lambda e: EventPersonDetail.objects.get_or_create(event=e)[0])


# class PersonRequest(models.Model):

class PageScheme(models.Model):
    event = models.ForeignKey(Event)
    theme = models.CharField(max_length=20, blank=True, null=True)
    scheme = models.TextField(blank=True, null=True)
    main = models.TextField(blank=True, null=True)
    nav = models.TextField(blank=True, null=True)
    footer = models.TextField(blank=True, null=True)


class Ticket(models.Model):
    name = models.CharField(_('name'), max_length=100, blank=False, null=False)
    description = models.CharField(_('description'), max_length=100, blank=True, null=True)
    quantity = models.IntegerField(_('quantity'), default=0)
    cost = models.IntegerField(_('cost'), blank=False, null=False)
    is_active = models.BooleanField(_('active'), default=True)
    is_public = models.BooleanField(_('public'), default=True)
    event = models.ForeignKey(Event)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Person(models.Model):
    first_name = models.CharField(max_length=50, null=False, blank=False)
    last_name = models.CharField(max_length=50, null=False, blank=False)
    email = models.EmailField(null=False, blank=False)
    fb_id = models.CharField(max_length=50)
    user = models.OneToOneField(User, blank=True, null=True)
    facebook = models.URLField(max_length=255, blank=True, null=True)
    linkedin = models.URLField(max_length=255, blank=True, null=True)
    twitter = models.URLField(max_length=255, blank=True, null=True)
    is_active = models.BooleanField(_('is active'), default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.first_name + " " + self.last_name


User.person = property(lambda s: Person.objects.get_or_create(user=s)[0])


class PersonInstitution(models.Model):
    person = models.ForeignKey(Person)
    institution = models.ForeignKey(Institution)

    code = models.CharField(max_length=20, blank=True, null=True)
    is_active = models.BooleanField(_('is active'), default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class PersonDetail(models.Model):
    man = _('man')
    woman = _('woman')

    SEX_CHOICES = (
        (1, man),
        (2, woman),
    )

    person = models.ForeignKey(Person)
    dni = models.CharField(_('DNI'), max_length=20, blank=True, null=True)
    document = models.CharField(max_length=50, null=True, blank=True)
    date_birth = models.DateField(_('Date Birth'), blank=True, null=True)
    sex = models.IntegerField(_('Sex'), blank=True, null=True, choices=SEX_CHOICES)
    profession = models.CharField(_('Profession'), max_length=100, null=True, blank=True)

Person.detail = property(lambda p: Person.objects.get_or_create(person=p)[0])


class EventPersonDetail(models.Model):
    event = models.ForeignKey(Event)
    is_dni = models.BooleanField(_('Request DNI'), default=False)
    is_date_birth = models.BooleanField(_('Request Date Birth'), default=False)
    is_sex = models.BooleanField(_('Request Sex'), default=False)
    is_institution = models.BooleanField(_('Request Institution'), default=False)
    is_code = models.BooleanField(_('Request Institution Code'), default=False)
    is_profession = models.BooleanField(_('Request Profession'), default=False)


class Register(models.Model):
    STATUS_CHOICES = (
        (1, _('pending')),
        (2, _('confirmed')),
    )

    person = models.ForeignKey(Person)
    person_institution = models.ForeignKey(PersonInstitution, blank=True, null=True)
    ticket = models.ForeignKey(Ticket, blank=True, null=True)
    event = models.ForeignKey(Event, blank=False, null=False)
    voucher = models.CharField(_('voucher'), max_length=100, blank=True, null=True)
    status = models.IntegerField(choices=STATUS_CHOICES, default=1)
    is_active = models.BooleanField(_('is active'), default=False)
    has_material = models.BooleanField(_('has material'), default=False)
    note = models.TextField(blank=True, null=True)
    note2 = models.TextField(blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


def content_file_name(instance, filename):
    return '/'.join(['events', instance.event.nick, filename])


class EventImage(models.Model):
    event = models.ForeignKey(Event, related_name='images')
    image = models.ImageField(upload_to=content_file_name)
