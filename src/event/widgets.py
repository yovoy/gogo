from django import forms
from django.utils.safestring import mark_safe
from sorl.thumbnail import get_thumbnail


class AvatarWidget(forms.FileInput):
    def __init__(self, attrs={}):
        super(AvatarWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        output = []
        if value and hasattr(value, "url"):
           img_path = get_thumbnail(value, 'x200', format='PNG')
           output.append((
                '<img src="%s" class="img-rounded" style="height: 200px" /><br/><br/>'
                          % (img_path.url)))
        html = '<div class="fileupload fileupload-new" data-provides="fileupload">' \
                 '<span class="btn btn-default btn-file">' \
                 '<span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select file</span>' \
                 '<span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>'
        output.append(html)
        output.append(super(AvatarWidget, self).render(name, value, attrs))
        html = '</span>' \
                '<span class="fileupload-preview" style="margin-left:5px;"></span>' \
                '<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>' \
                '</div>'
        output.append(html)

        return mark_safe(u''.join(output))


class AvatarWidgetNoLink(forms.FileInput):
    def __init__(self, attrs={}):
        super(AvatarWidgetNoLink, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        output = []
        if value and hasattr(value, "url"):
           img_path = get_thumbnail(value, '200x200', format='PNG')
           output.append((
                '<img src="%s" class="img-rounded" /><br/><input type="checkbox" name="clean_image"> Quitar imagen <br/>'
                          % (img_path.url)))
        html = ''
        output.append(html)
        output.append(super(AvatarWidgetNoLink, self).render(name, value, attrs))
        html = ''
        output.append(html)

        return mark_safe(u''.join(output))


'''
This Widget if made for Bootstrap Only
'''
class EmailDomainWidget(forms.TextInput):
    def __init__(self, attrs=None):
        if 'domain' in attrs:
            self.domain = attrs['domain']
            del attrs['domain'];
        super(EmailDomainWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        output = []
        output.append('<div class="input-group">')
        output.append(super(EmailDomainWidget, self).render(name, value, attrs))
        output.append('<span class="input-group-addon">')
        output.append('@' + self.domain)
        output.append('</span></div>')

        return mark_safe(u''.join(output))
