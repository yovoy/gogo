var geocoder;
var jCoreGoogleMap = {
    maps: {},
    styles: {
        // from http://gmaps-samples-v3.googlecode.com/svn/trunk/styledmaps/wizard/index.html
        base: [
            //{ "featureType": "road.local", "elementType": "geometry.fill", "stylers": [ { "color": "#36ed3b" } ]},
            //{ "featureType": "road.arterial", "elementType": "geometry.fill", "stylers": [ { "hue": "#00ff4d" }, { "color": "#df8080" } ] }
        ]
    },

    /**
     * Drop a marker on the map's current center
     *
     * If the bindMarkerTo variable is set then the
     * marker will be draggable
     */
    dropMarker: function (sender, mapId, keepZoom) {
        if (!this.googleMapsLoaded()) return;
        var map = this.maps[mapId];
        if (!map) return;
        if (map.marker != null) {
            map.marker.setPosition(map.map.getCenter());
        }
        else {
            map.marker = new google.maps.Marker({
                position: map.map.getCenter(),
                map: map.map
            });
            if (map.bindMarkerTo) {
                map.marker.setDraggable(true);
                var self = this;
                google.maps.event.addListener(map.marker, 'dragend', function () {
                    self.bindMarker(null, mapId);
                });
            }
        }
        if (!keepZoom)
            map.map.setZoom(16);
        this.bindMarker(null, mapId);
    },

    /**
     * Allow locating the current position of a user. Once
     * located the center is changed to that position.
     */
    locateMe: function (sender, mapId) {
        if (!this.googleMapsLoaded()) return;
        var map = this.maps[mapId];
        if (!map) return;
        if (!navigator || !navigator.geolocation) {
            alert('Not supported by your browser');
            return;
        }
        $(sender).attr('disabled', 'disabled');
        var self = this;
        navigator.geolocation.getCurrentPosition(
            function (pos) {
                $(sender).removeAttr('disabled');
                var latlng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
                self.setCenter(mapId, latlng);
                map.map.setZoom(15);
                geocoder.geocode({'latLng': latlng}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        self.setSearchBoxVal(mapId, results[1].formatted_address)
                    }
                });

            },
            function (e, e2) {
                $(sender).removeAttr('disabled');
                if (e && e.message)
                    alert(e.message);
            }
        );
    },

    /**
     * Search an address for a specific map.
     * Search will be performed 3 seconds after called
     * but will always get the new search string
     */
    searchAndCenter: function (str, cityName, mapId, sender, disableSearchIfMapMarked) {
        if (!this.googleMapsLoaded()) return;
        var map = this.maps[mapId];
        if (!map) return;
        if (disableSearchIfMapMarked && map.marker)
            return;
        str = $.trim(str);
        if (str == map.__searchString && map.__searchCityName == cityName)
            return;
        map.__searchString = str;
        map.__searchCityName = cityName;

        // If nothing to search clear
        if (map.__searchString == "") {
            if (map.__searchTimeout) {
                clearTimeout(map.__searchTimeout);
                map.__searchTimeout = null;
            }
            if (map.__searchIndicator) {
                map.__searchIndicator.remove();
                map.__searchIndicator = null;
            }
            return;
        }


        // If timeout already set then clear
        if (map.__searchTimeout) {
            return;
        }

        // Create timeout
        var waitTime = 3000;
        var self = this;
        map.__searchTimeout = setTimeout(function () {
            self.__performSearch(mapId);
            //map.__searchString = "";
            //map.__searchCityName = "";
            map.__searchTimeout = null;
            if (map.__searchIndicator)
                map.__searchIndicator.remove();
            map.__searchIndicator = null;
        }, waitTime);

        map.__searchIndicator = $("<div/>");
        map.__searchIndicator.css({
            'position': 'absolute',
            'width': '1px',
            'height': '1px',
        });
        var icon = $("<span class='glyphicon glyphicon-refresh glyphicon-spin'/>");
        icon.css({
            'position': 'relative',
            'left': $(sender).outerWidth() - 20,
            'top': ($(sender).outerHeight() / 2 - 10)
        });
        icon.appendTo(map.__searchIndicator);
        map.__searchIndicator.insertBefore(sender);
    },

    /**
     * Method called by searchAndCenter to fetch
     * for the most accurate location for the
     * search string
     */
    __performSearch: function (mapId) {
        var map = this.maps[mapId];
        if (!map) return;

        var url = "http://maps.googleapis.com/maps/api/geocode/json?address=%QUERY&sensor=true";
        url = url.replace("%QUERY", encodeURIComponent(map.__searchString + " " + map.__searchCityName));

        var self = this;
        if (window.XDomainRequest) {
            var xdr = new XDomainRequest();
            xdr.open("get", url);
            xdr.onload = function () {
                var res = $.parseJSON(xdr.responseText);
                if (res.status == "OK") {
                    res = res.results[0];
                    if (res) {
                        self.setCenter(mapId, [res.geometry.location.lat, res.geometry.location.lng]);
                        map.map.setZoom(15);
                    }
                }
            };
            xdr.send();
        }
        else {
            $.ajax({
                url: url,
                dataType: 'json'
            })
                .done(function (res) {
                    if (res.status == "OK") {
                        res = res.results[0];
                        if (res) {
                            self.setCenter(mapId, [res.geometry.location.lat, res.geometry.location.lng]);
                            map.map.setZoom(15);
                        }
                    }
                })
                .fail(function (xhr, textStatus, err) {
                    var str = "";
                    str += xhr.readyState + "<br/>";
                    str += xhr.responseText + "<br/>";
                    str += xhr.status + "<br/>";
                    str += textStatus + "<br/>";
                    str += err + "<br/>";
                    document.write(str);
                });
        }
    },

    /**
     * Change the center of a specific map.
     * The center can be an array, string, LatLng or object
     * with latitude and longitude
     */
    setCenter: function (mapId, center, zoom) {
        if (!this.googleMapsLoaded()) return;
        var map = this.maps[mapId];
        if (!map) return;
        if (center == null) return;
        if (typeof(center) == "string") {
            var parts = center.split(",");
            center = new google.maps.LatLng(parts[0], parts[1]);
        }
        else if (center.length) {
            center = new google.maps.LatLng(center[0], center[1]);
        }
        else if (center.latitude) {
            center = new google.maps.LatLng(center.latitude, center.longitude);
        }
        map.map.setCenter(center);
        if (zoom) {
            map.map.setZoom(15);
        }
    },

    setSearchBoxVal: function (mapId, val) {
        $('#' + mapId + '_searchbox').val(val);
    },

    /**
     * Bind the marker position of a map into the
     * input field specified by bindMarkerTo
     */
    bindMarker: function (sender, mapId) {
        var map = this.maps[mapId];
        if (!map) return;
        if (!map.bindMarkerTo) return;
        var pos = map.marker.getPosition().lat() + "," + map.marker.getPosition().lng();
        $(map.bindMarkerTo).val(pos);
    },

    /**
     * Once google maps starts then init everything
     * on this class
     */
    init: function () {
        geocoder = new google.maps.Geocoder();
        // Init styles
        for (var id in this.styles) {
            var style = this.styles[id];
            this.styles[id] = new google.maps.StyledMapType(style, {name: id});
        }

        // Init the maps
        for (var id in this.maps) {
            this.__initMap(id);
            var input = document.getElementById(id + '_searchbox');
            if (input) {
                var searchBox = new google.maps.places.SearchBox(input);
                google.maps.event.addListener(searchBox, 'places_changed', function () {
                    var places = searchBox.getPlaces();

                    if (places.length == 0) {
                        return;
                    }
                    var place = places[0];
                    jCoreGoogleMap.setCenter(id, [place.geometry.location.lat(), place.geometry.location.lng()], 15);
                });
                if (input.value != "") {
                    geocoder.geocode({'address': input.value }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            result = results[0];
                            jCoreGoogleMap.setCenter(id, [result.geometry.location.lat(), result.geometry.location.lng()], 15);
                        }
                    });
                    google.maps.event.trigger(input, 'place_changed');
                }
            }
        }
    },

    /**
     * Init a single map from the maps array
     */
    __initMap: function (id) {
        var map = this.maps[id];
        if (map.__initialized) return;
        map.__initialized = true;

        // Make sure the map center is a LatLng point
        map.options.center = new google.maps.LatLng(map.options.center.latitude, map.options.center.longitude);

        // Create the map
        map.map = new google.maps.Map(document.getElementById(id), map.options);
        map.map.mapTypes.set('default_style', this.styles.base);
        map.map.setMapTypeId('default_style');

        if (map.marker == 'center') {
            map.marker = null;
            this.dropMarker(null, id);
        }
    },

    /**
     * Attach a new map to the plugin.
     * If google already loaded then init the map automatically
     */
    attach: function (mapId, setup) {
        if (this.googleMapsLoaded()) {
            this.maps[mapId] = setup;
            this.__initMap(mapId);
            console.log('zas')
        }
        else {
            this.maps[mapId] = setup;
        }
    },

    /**
     * Determine if google maps has already
     * been loaded
     */
    googleMapsLoaded: function () {
        return (window.google && window.google.maps);
    },

    /**
     * Load Google Maps if not loaded already.
     * It already loaded then init the maps that
     * are not initted already.
     */
    loadGoogleMaps: function (c) {
        if (this.googleMapsLoaded())
            this.init();
        else {
            var callback = 'jCoreGoogleMap.init';
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&sensor=false&callback=' + callback;

            document.body.appendChild(script);
        }
    }
};