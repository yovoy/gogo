import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from django.utils.translation import ugettext as _

from event.forms import EventForm, EventEditForm, EventImageForm, TicketForm, PersonForm
from event.functions import get_array_dict_of_post_array, get_event_or_404_by_slug, get_fb_app_id, get_datatable_data, \
    date_handler
from event.models import Ticket, Event, Register, Person
from gogo import settings


@login_required
def event_create(request):
    title = _('create new event')
    layout = 'event/layout.html'
    if request.POST:
        tickets = get_array_dict_of_post_array(request.POST, {"name", "quantity", "cost"})
        form = EventForm(request.POST)
        if form.is_valid():
            event = form.save(commit=False)
            event.owner = request.user
            event.save()

            page_scheme = event.page_scheme
            page_scheme.main = settings.EVENT_MAIN.replace('EVENT_NICK', event.nick)
            page_scheme.nav = settings.EVENT_NAV
            page_scheme.scheme = settings.EVENT_SCHEME.replace('EVENT_NICK', event.nick)

            page_scheme.save()

            if tickets:
                for ticket in tickets:
                    a = Ticket(**ticket)
                    a.event = event
                    a.cost = int(str(a.cost).replace('.', ','))
                    a.quantity = int(str(a.quantity).replace('.', ','))
                    a.save()
            return redirect(reverse('event:home', args=[event.nick]))
    else:
        form = EventForm()
    return render(request, 'event/event_form.html', locals())


@login_required
def event_edit(request, event_nick):
    title = _('edit event')
    layout = 'event/layout_event.html'
    event = get_event_or_404_by_slug(event_nick)

    if request.POST:
        form = EventEditForm(request.POST, request.FILES, instance=event)
        if form.is_valid():
            event = form.save(commit=False)
            if request.POST.get('clean_image'):
                event.logo = None
            domain = event.domain
            if domain:
                domain = domain.replace('www.', '')
                if domain.endswith('/'):
                    domain = domain[:-1]
                event.domain = domain
            event.save()
            form = EventEditForm(instance=event)
            messages.add_message(request, messages.INFO, _('<strong>Success!</strong> your changes has been saved.'))
    else:
        form = EventEditForm(instance=event)
    return render(request, 'event/event_form_edit.html', locals())


def home(request, event_nick):
    event = get_event_or_404_by_slug(event_nick)
    FB_APP_ID = get_fb_app_id()
    if not event.slogan:
        event.slogan = _("We're gonna make one of those blurry cafe videos and everything, so don't miss out!")

    if not event.description:
        event.description = _("Bringing together the web's most forward thinking innovators")

    return render(request, 'event/variant.html', locals())


def show_panel(request):
    events = Event.objects.filter(owner=request.user)
    return render(request, 'event/panel.html', locals())


@csrf_exempt
def scheme(request, event_nick):
    event = get_event_or_404_by_slug(event_nick)
    if request.POST:
        page_scheme = event.page_scheme

        page_scheme.scheme = request.POST.get('scheme')
        page_scheme.theme = request.POST.get('theme')
        page_scheme.main = request.POST.get('main')
        page_scheme.nav = request.POST.get('nav')
        # page_scheme.footer = request.POST.get('footer')

        page_scheme.save()

    response_data = {'success': 'ok'}
    return HttpResponse(json.dumps(response_data), content_type="application/json")


@csrf_exempt
def upload_image(request):
    form = EventImageForm(request.POST, request.FILES)
    if form.is_valid():
        event_image = form.save()
        response_data = {'success': 'ok', 'url': event_image.image.url, 'name': event_image.image.name}
    else:
        response_data = {'success': 'false'}
    return HttpResponse(json.dumps(response_data), content_type="application/json")


@csrf_exempt
def save_ticket(request):
    id = request.POST.get('id')
    if id:
        form = TicketForm(request.POST, instance=get_object_or_404(Ticket, pk=id))
    else:
        form = TicketForm(request.POST)
    if form.is_valid():
        ticket = form.save()
        response_data = {'success': 'true', 'id': ticket.id}
    else:
        errors = form._errors
        response_data = list(errors)
    return HttpResponse(json.dumps(response_data), content_type="application/json")


@csrf_exempt
def delete_ticket(request):
    id = request.POST.get('id')
    response_data = {}
    if id:
        ticket = get_object_or_404(Ticket, pk=id)
        if Register.objects.filter(ticket=ticket).count() == 0:
            ticket.delete()
            response_data = {'success': 'true'}
    return HttpResponse(json.dumps(response_data), content_type="application/json")


@csrf_exempt
# @login_required()
def registers_json(request, event_nick):
    end, field_order_by, query, start = get_datatable_data(request)
    event = get_event_or_404_by_slug(event_nick)
    registers = Register.objects.values('id', 'ticket__name', 'person__first_name', 'person__last_name',
                                        'voucher', 'status',
                                        'created_at').filter(event=event)

    if query == 'query..':
        pass

    registers = registers.order_by(field_order_by)

    iTotalDisplayRecords = registers.count()

    if end:
        registers = registers[start:end]

    result = {'aaData': list(registers), 'iTotalRecords': registers.count(),
              'iTotalDisplayRecords': iTotalDisplayRecords}

    return HttpResponse(json.dumps(result, default=date_handler), content_type='application/json')


def registers(request, event_nick):
    event = get_event_or_404_by_slug(event_nick)
    return render(request, 'event/registers.html', locals())


@csrf_exempt
# @login_required()
def registers_jqgrid(request, event_nick):
    event = get_event_or_404_by_slug(event_nick)
    rows = Register.objects.values('id', 'created_at', 'updated_at', 'person__id', 'person__first_name',
                                   'person__last_name',
                                   'person__email',
                                   'ticket__name', 'ticket__id', 'voucher', 'has_material', 'note', 'note2', 'status',
                                   'is_active', ).filter(event=event)

    result = {
        'records': 10,
        'page': 1,
        'total': 1,
        'rows': list(rows)
    }
    return HttpResponse(json.dumps(result, default=date_handler), content_type='application/json')


@csrf_exempt
def registers_jqgrid_edit(request, event_nick):
    oper = request.POST.get("oper")

    result = {}
    if oper is "del":
        pass
    else:
        person_posted = get_post_data_start_with(request, 'person__')
        person = get_object_or_404(Person, pk=request.POST.get('person__id'))
        person_form = PersonForm(person_posted, instance=person)
        if person_form.is_valid():
            person_form.save()
        else:
            errors = person_form.errors
            for error, val in errors.items():
                result['error'] = error + ' - ' + val[0]
                break
            return HttpResponse(json.dumps(result, default=date_handler), content_type='application/json')

    return HttpResponse(json.dumps(result, default=date_handler), content_type='application/json')


def get_post_data_start_with(request, start_with):
    data = {}
    keys = [k for k, v in request.POST.items() if k.startswith(start_with)]
    for key in keys:
        data[key.replace(start_with, '')] = request.POST.get(key)
    # return json.dumps(data)
    return data
