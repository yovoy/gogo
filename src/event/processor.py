from django.template import RequestContext, Context
from event.models import Event

__author__ = 'bicho'

def events_for_current_user(request):
    if not request.user.is_authenticated():
        return []
    return {'events_for_current_user': Event.objects.filter(owner=request.user) }

def nickname(request):
    return {'nickname':'bicho'}