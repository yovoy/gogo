from django.contrib import admin

# Register your models here.
from event.models import Event, Register, Ticket, PageScheme

admin.site.register(Event)
admin.site.register(Ticket)
admin.site.register(Register)
admin.site.register(PageScheme)
