/**
 * Created by bicho on 21/04/2015.
 */
accessToken='';
window.fbAsyncInit = function () {
    FB.init({
        appId: fbAppId,
        oauth: true,
        status: true, // check login status
        cookie: true, // enable cookies to allow the server to access the session
        xfbml: true, // parse XFBML
        version: 'v2.2'
    });

    $.get("https://graph.facebook.com/oauth/access_token?" +
            "client_id=382068031965084" +
            "&client_secret=da954919faee35b9f1acc2fe14272c36" +
            "&grant_type=client_credentials",
        function (data) {
            accessToken = data.split("=")[1];
            if (eval("typeof afterFb_asyncInit == 'function'")) {
                afterFb_asyncInit();
            }
        });
};
function fb_login(callback) {
    FB.login(function (response) {
        if (response.authResponse) {
            console.log('Welcome!  Fetching your information.... ');
            //console.log(response); // dump complete info
            access_token = response.authResponse.accessToken; //get access token
            user_id = response.authResponse.userID; //get FB UID

             FB.api('/me?fields=email,last_name,first_name', function (fbResponse) {
                callback(fbResponse)
            });

        } else {
            //user hit cancel button
            console.log('User cancelled login or did not fully authorize.');

        }
    }, {
        scope: 'public_profile, email'
    });
    return false;
}

function doFb_feed(fbId, limit) {
    FB.api('/'+fbId+'/feed?limit=' + limit, 'get', {'access_token': accessToken }, function (fbResponse) {
        if (eval("typeof afterFb_posts == 'function'")) {
            afterFb_posts(fbResponse.data);
        }
    });
}

function doFb_posts(fbId, limit) {
    FB.api('/'+fbId+'/posts?limit=' + limit, 'get', {'access_token': accessToken }, function (fbResponse) {
        if (eval("typeof afterFb_posts == 'function'")) {
            afterFb_posts(fbResponse.data);
        }
    });
}

function doFb_photos(fbId, limit) {
    FB.api('/'+fbId+'/photos?limit=' + limit, 'get', {'access_token': accessToken }, function (fbResponse) {
//    FB.api('/'+fbId+'/photos?limit=' + limit, 'get', {'access_token': accessToken }, function (fbResponse) {
        if (eval("typeof afterFb_photos == 'function'")) {
            afterFb_photos(fbResponse.data);
        }
    });
}
(function () {
    var e = document.createElement('script');
    e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
    e.async = true;
    document.getElementById('fb-root').appendChild(e);
}());