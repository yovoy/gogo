/**
 * Created by bicho on 21/04/2015.
 */

var monthShortNames = new Array("Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic");
function afterFb_asyncInit() {

//    if (fbEventId) {
//        if(fbType){
//            doFb_feed(fbEventId, 5);
//        }else{
//            doFb_posts(fbEventId, 5);
//        }
//        if (fbAlbumId != '') {
//            doFb_photos(fbAlbumId, 5);
//        } else {
//            doFb_photos(fbEventId, 5);
//        }
//    }
}

function afterFb_posts(fbResponse) {
    $('.news').remove();
    for (var i = 0; i < fbResponse.length; i++) {
        var post = fbResponse[i];
//                    console.log(post);
        if (post.message) {
            var formattedDate = new Date(post.created_time);
            var postNew =
                '<li class="news">' +
                '<span class="news-date">' + monthShortNames[formattedDate.getMonth() + 1] + ' ' + formattedDate.getDate() + '</span>' +
                '<a target="_blank" href="' + post.link + '"><h4>' + post.message + '</h4></a>' +
                '</li>';
            $('.news-list').append(postNew);
        }
    }
}
function afterFb_feed(fbResponse) {
    $('.news').remove();
    for (var i = 0; i < fbResponse.length; i++) {
        var post = fbResponse[i];
//                    console.log(post);
        if (post.message) {
            var formattedDate = new Date(post.created_time);
            var postNew =
                '<li class="news">' +
                '<span class="news-date">' + monthShortNames[formattedDate.getMonth() + 1] + ' ' + formattedDate.getDate() + '</span>' +
                '<a target="_blank" href="' + post.link + '"><h4>' + post.message + '</h4></a>' +
                '</li>';
            $('.news-list').append(postNew);
        }
    }
}

function afterFb_photos(fbResponse) {
    $('#slider').html('');
    var photos = [];
    for (var i = 0; i < fbResponse.length; i++) {
        var photo = fbResponse[i];
        var image300 = photo.images[4],
            comments = $('<div/>'),
            formattedDate = new Date(photo.created_time),
            photoName = photo.name ? photo.name : '';
        var j;
        for (j = 0; j < photo.images.length; j++) {
            if (photo.images[j].height > 299 && photo.images[j].height < 400) {
                image300 = photo.images[j];
                break;
            }
        }
        if (photo.comments) {
            var photoComments = photo.comments.data;
            for (j = 0; j < photoComments.length; j++) {
                var photoComment = photoComments[j];
                var comment =
                    '<div class="media" style="text-align: right">' +
                    '    <div class="media pull-right">' +
                    '        <a href="">' +
                    '            <img class="media-object" src="http://graph.facebook.com/v2.3/' + photoComment.from.id + '/picture?type=small" alt="' + photoComment.from.name + '">' +
                    '        </a>' +
                    '    </div>' +
                    '    <div class="media-body">' +
                    '        <h4 class="media-heading">' + photoComment.message + '</h4>' +
                    photoComment.from.name +
                    '   </div>';
                '</div>';
//                comment.append(photoComment.from.name);
                comments.append(comment);
            }
        }
        var photoSlide =
            '<div class="item">' +
            '<div class="row">' +
            '<div class="col-md-6">' +
            '<img src="' + image300.source + '" alt="' + photoName + '">' +
            '</div>' +
            '<div class="col-md-6">' + photoName + '<br/>' + comments.html() + '</div>' +
            '</div>' +
            '</div>';
        var photoSlide = '<img src="' + image300.source + '" alt="' + photo.name + '">';
        $('#slider').append(photoSlide);

    }


    var owl = $("#slider");
    owl.owlCarousel({
//        theme: "slider",
        navigation: true,
        pagination: false,
        singleItem: true,
        slideSpeed: 400,
        mouseDrag: false
    });
}

function afterFb_me(fbResponse) {
    $.post('/accounts/get-or-create-user-with-fb/', {data: JSON.stringify(fbResponse)}, function (response) {
        if (response.redirect_to) {
            window.location.href = response.redirect_to;
        } else {
            alert('intenta de nuevo porfavor');
        }
    });
}

