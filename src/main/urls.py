from django.conf.urls import patterns
from django.conf.urls import url


urlpatterns = patterns('main.views',
                       url(r'^(?P<event_nick>.+)/register/$', 'register', name='register'),
                       url(r'^(?P<event_nick>.+)/registers/$', 'filter_register', name='filter_register'),
                       url(r'^(?P<event_nick>.+)/$', 'show_event', name='show_event'),
                       url(r'^$', 'home', name='home'),
                       )
