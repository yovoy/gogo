from django import forms
from event.models import PersonDetail

__author__ = 'bicho'

from django.utils.translation import ugettext as _
from accounts.models import Person
from event.functions import add_class
from django.forms import ModelForm


class PersonForm(ModelForm):
    form_meta = {
        'title': _('new person'),
        'help_collapse': True,
    }

    class Meta:
        model = Person
        fields = ('first_name', 'last_name')

    def __init__(self, *args, **kwargs):
        super(PersonForm, self).__init__(*args, **kwargs)
        add_class(self, ['first_name', 'last_name'])

        if 'instance' in kwargs:
            self.form_meta['title'] = _('edit') + ' ' + _('person')
        else:
            self.form_meta['title'] = _('new person')


class PersonDetailForm(ModelForm):
    extra = {}

    class Meta:
        model = PersonDetail
        exclude = '__all__'

    def __init__(self, *args, **kwargs):
        extra = kwargs.pop('extra')
        super(PersonDetailForm, self).__init__(*args, **kwargs)

        for i, question in enumerate(extra):
            self.fields['custom_%s' % i] = forms.CharField(label=question)
