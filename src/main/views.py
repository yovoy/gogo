import json
from django.http import HttpResponse, Http404
from django.shortcuts import render, get_object_or_404

# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from accounts.forms import FacebookUserForm
from accounts.functions import get_or_create_user_with_fb_util, FOUND, CREATED
from event.functions import get_event_or_404_by_slug, get_fb_app_id
from event.models import Register, Ticket


def home(request):
    event = request.event
    if event:
        return show_event(request, event.nick)

    FB_APP_ID = get_fb_app_id()
    # return render_to_response('events/landing_event.html')
    return render(request, 'main/home.html', locals())


def show_event(request, event_nick):
    FB_APP_ID = get_fb_app_id()
    event = get_event_or_404_by_slug(event_nick)
    title = event.nick
    page_scheme = event.page_scheme
    scheme = page_scheme.scheme

    tickets = Ticket.objects.values('id', 'name').filter(event=event, is_active=True, is_public=True)

    event_parameters = {
        "tickets": list(tickets)
    }

    scheme = scheme.replace('TICKETS_JSON', json.dumps(event_parameters))
    return render(request, 'main/landing_event.html', locals())


@csrf_exempt
def register(request, event_nick):
    event = get_event_or_404_by_slug(event_nick)
    if not request.POST:
        raise Http404

    facebook_user_form = FacebookUserForm(request.POST)
    if not facebook_user_form.is_valid():
        raise 404
    response, user = get_or_create_user_with_fb_util(facebook_user_form=facebook_user_form)

    result = 0
    if response == FOUND or response == CREATED:
        ticket_id = request.POST.get('ticket_id')
        ticket = None
        if ticket_id:
            ticket = get_object_or_404(Ticket, pk=ticket_id)
        try:
            register = Register.objects.get(person=user.person, event=event)
            if ticket:
                register.ticket = ticket
                register.save()
            result = 2
        except Register.DoesNotExist:

            register = Register(person=user.person, event=event)
            if ticket:
                register.ticket = ticket
            register.save()
            result = 1
        except Register.MultipleObjectsReturned:
            result = 3

    return HttpResponse(result)


def filter_register(request, event_nick):
    event = get_event_or_404_by_slug(event_nick)
