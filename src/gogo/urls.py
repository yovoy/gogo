from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.static import static
from gogo import settings

urlpatterns = [
    url(r'^panel/', 'event.views.show_panel', name='show_panel'),
    url(r'^evento/', include('event.urls', namespace='event')),
    url(r'^accounts/', include('accounts.urls', namespace='accounts')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('main.urls', namespace='main')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

