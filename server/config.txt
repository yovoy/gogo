
<VirtualHost *:80>
        ServerName yovoy.aqpup.com
        WSGIScriptAlias / /srv/gogo/src/gogo/gogo/wsgi.py process-group=yovoy.aqpup.com
        WSGIDaemonProcess clean.aqpup.com python-path=/srv/gogo/src/gogo/:/srv/oficlean/virtualenv/oficlean/lib/python3.4/site-packages
        WSGIProcessGroup yovoy.aqpup.com

        <Directory /srv/gogo/src/gogo/>
                <Files wsgi.py>
                        Require all granted
                </Files>
        </Directory>

        Alias /static /srv/gogo/src/gogo/static
        <Directory /srv/gogo/src/gogo/static>
               Require all granted
        </Directory>
</VirtualHost>
